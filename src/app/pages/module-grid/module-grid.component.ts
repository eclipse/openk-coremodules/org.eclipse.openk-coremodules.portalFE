/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { SessionContext } from '@common/session-context';
import { UserModule } from '@model/user-module';
import { AuthenticationService } from '@services/authentication.service';
import { UserModuleService } from '@services/user-module.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-module-grid',
  templateUrl: './module-grid.component.html',
  styleUrls: ['./module-grid.component.css'],
})
export class ModuleGridComponent implements OnInit {
  @Input() moduleRights = true;
  userModules: UserModule[] = [];
  roles: string[] = [];

  constructor(
    private sessionContext: SessionContext,
    private userModuleService: UserModuleService,
    private authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.getUserModules();
  }

  goToModuleIfValid(userModule: UserModule): void {
    const elogbookLink = userModule.link + '?accessToken=' + this.sessionContext.getAccessToken();

    this.authService
      .checkAuth()
      .pipe(take(1))
      .subscribe(() => {
        window.open(elogbookLink, '_blank');
      });
  }

  getUserModules(): void {
    this.userModuleService
      .getUserModulesForUser()
      .pipe(take(1))
      .subscribe(userModuleResult => {
        this.setModulesForRole(userModuleResult);
      });
  }

  setModulesForRole(userModuleResult: UserModule[]): void {
    const accessTokenDecoded = this.sessionContext.getAccessTokenDecoded();
    if (accessTokenDecoded) {
      this.roles = accessTokenDecoded.roles;
    } else {
      throw new Error('No access token decoded found!');
    }

    for (const umr of userModuleResult) {
      if (this.roles.indexOf(umr.requiredRole) !== -1) {
        this.userModules.push(umr);
        this.moduleRights = true;
      }
    }
    if (this.userModules.length === 0) {
      // eslint-disable-next-line no-console
      console.warn('Der Benutzer hat keine Rechte');
      this.moduleRights = false;
    }
  }

  @HostListener('window:focus', ['$event'])
  onFocus(): void {
    this.authService.checkAuth().pipe(take(1)).subscribe();
  }
}
