/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { SessionContext } from '@common/session-context';
import { LoginCredentials } from '@model/login-credentials';
import { AuthenticationService } from '@services/authentication.service';
import { JwtToken } from 'app/model/jwt-token';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  public showError = false;
  private urlParamsMap: ParamMap | null;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    public sessionContext: SessionContext,
    private activatedRoute: ActivatedRoute
  ) {
    this.urlParamsMap = this.activatedRoute.snapshot.queryParamMap;
  }

  private pastLogin(): void {
    this.processDirectLinkToGridMeasureDetail();
    this.goToOverview();
  }

  private processDirectLinkToGridMeasureDetail(): void {
    if (!this.urlParamsMap) {
      return;
    }
    const fwdUrl = this.urlParamsMap.get('fwdUrl');
    const fwdId = this.urlParamsMap.get('fwdId');
    if (!fwdUrl) {
      return;
    }

    const linkToOpen = fwdUrl + '?accessToken=' + this.sessionContext.getAccessToken() + '&fwdId=' + fwdId;
    this.urlParamsMap = null;

    this.authService
      .checkAuth()
      .pipe(take(1))
      .subscribe(() => {
        window.open(linkToOpen, '_blank');
      });
  }

  private setError(showErr: boolean): void {
    this.showError = showErr;
  }

  public login(name: string, pw: string): void {
    this.setError(false);
    const creds = new LoginCredentials();
    creds.userName = name;
    creds.password = pw;
    this.authService
      .login(creds)
      .pipe(take(1))
      .subscribe(
        (jwtToken: JwtToken) => this.onLoggedInSuccessfully(jwtToken),
        error => {
          this.setError(true);
          throw new Error(error);
        }
      );
  }

  private onLoggedInSuccessfully(jwtToken: JwtToken): void {
    this.sessionContext.setAccessToken(jwtToken.access_token);
    this.pastLogin();
  }

  public goToOverview(): void {
    this.router.navigate(['/overview']);
  }
}
