/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Globals } from '@common/globals';
import { SessionContext } from '@common/session-context';
import { UserModule } from '@model/user-module';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseHttpService } from './base-http.service';

@Injectable()
export class UserModuleService extends BaseHttpService {
  constructor(
    private http: HttpClient,
    private sessionContext: SessionContext
  ) {
    super();
  }

  public getUserModulesForUser(): Observable<UserModule[]> {
    const headers = this.createCommonHeaders(this.sessionContext);

    return this.http
      .get<UserModule[]>(Globals.BASE_PORTAL_URL + '/userModulesForUser/', {
        headers: headers,
        observe: 'response',
      })
      .pipe(
        map((response: HttpResponse<UserModule[]>) => super.extractData(response, this.sessionContext)),
        catchError(err => super.handleErrorPromise(err, this.sessionContext))
      );
  }
}
