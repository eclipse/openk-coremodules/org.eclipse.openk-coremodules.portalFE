/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EventEmitter } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Globals } from '@common/globals';
import { SessionContext } from '@common/session-context';
import { UserModule } from '@model/user-module';
import { UserModuleService } from './user-module.service';

describe('UserModuleService', () => {
  let service: UserModuleService;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    const sessionContextMock = {
      getAccessToken: jasmine.createSpy('getAccessToken').and.returnValue('token'),
      getCurrSessionId: jasmine.createSpy('getCurrSessionId').and.returnValue('CurrSessionId'),
      centralHttpResultCode$: new EventEmitter<number>(), // Add this line
    };

    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserModuleService, { provide: SessionContext, useValue: sessionContextMock }],
    }).compileComponents();

    service = TestBed.inject(UserModuleService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Make sure that there are no outstanding requests
  });

  it('should get user modules for user', () => {
    const dummyUserModules: UserModule[] = [
      {
        name: 'Test',
        cols: 1,
        rows: 1,
        color: 'red',
        link: 'http://www.google.de',
        requiredRole: 'ADMIN',
      },
      {
        name: 'Test2',
        cols: 1,
        rows: 1,
        color: 'red',
        link: 'http://www.google.de',
        requiredRole: 'ADMIN',
      },
    ];

    service.getUserModulesForUser().subscribe(userModules => {
      expect(userModules.length).toBe(2);
      expect(userModules).toEqual(dummyUserModules);
    });

    const req = httpMock.expectOne(`${Globals.BASE_PORTAL_URL}/userModulesForUser/`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyUserModules);
  });
});
