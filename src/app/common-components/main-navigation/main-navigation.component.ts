/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SessionContext } from '@common/session-context';
import { LogoutComponent } from '@dialogs/logout/logout.component';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css'],
})
export class MainNavigationComponent {
  constructor(
    public sessionContext: SessionContext,
    public dialog: MatDialog,
    private router: Router
  ) {}

  openDialogLogout(): void {
    this.dialog.open(LogoutComponent, {
      width: '320px',
      height: '215px',
      disableClose: true,
    });
  }

  navigate(): void {
    if (this.sessionContext.getAccessToken()) {
      this.router.navigate(['/overview']);
    } else {
      this.router.navigate(['/login']);
    }
  }
}
