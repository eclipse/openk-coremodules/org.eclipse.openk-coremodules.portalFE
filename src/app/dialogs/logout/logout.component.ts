/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, Optional } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SessionContext } from '@common/session-context';
import { AuthenticationService } from '@services/authentication.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent {
  constructor(
    private readonly router: Router,
    private readonly sessionContext: SessionContext,
    private readonly authService: AuthenticationService,
    @Optional() public dialogRef: MatDialogRef<LogoutComponent>
  ) {}

  logout(): void {
    this.authService
      .logout()
      .pipe(take(1))
      .subscribe(() => {
        if (this.dialogRef) {
          this.dialogRef.close();
        }
        this.router.navigate(['/login']);
        this.sessionContext.clearStorage();
      });
  }
}
